/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import java.util.Scanner;

/**
 *
 * @author Daniel Acuña
 */
public class Calcu {
    
    public static void cuadrado(int a, int b){ 
        System.out.println("El area del cuadrado es: " + (a*b));  
    }
    public static void rectangulo(int a, int b){ 
        System.out.println("El area del Rectangulo es: " + (a*b));  
    }
    public static void triangulo(int a, int b){ 
        System.out.println("El area del triangulo es: " + ((a*b)/2));  
    }
    
    public static void main(String[] args){
        Scanner leer=new Scanner(System.in);
        int option=-1;
        Calcu obj = new Calcu();
        while(option!=4){
            System.out.println("CALCULADORA DE AREAS");
            System.out.println("1.- Cuadrado");
            System.out.println("2.- Rectangulo");
            System.out.println("3.- Triangulo");
            System.out.println("4.- Salir");
            
            System.out.println("Ingrese una opcion");
            
            option = leer.nextInt();
            int a,b;
            switch(option){
                case 1: 
                    System.out.println("Introduce el primer lado");
                    a = leer.nextInt();
                    System.out.println("Introduce el segundo lado");
                    b = leer.nextInt();
                    Calcu.cuadrado(a,b);
                    break;
                case 2: 
                    System.out.println("Introduce la base");
                    a = leer.nextInt();
                    System.out.println("Introduce la altura");
                    b = leer.nextInt();
                    Calcu.rectangulo(a,b);
                    break;
                case 3: 
                    System.out.println("Introduce su base");
                    a = leer.nextInt();
                    System.out.println("Introduce su altura");
                    b = leer.nextInt();
                    Calcu.triangulo(a,b);
                    break;
            }
            
        }
    }  
}
