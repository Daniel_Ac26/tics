#include <stdio.h>
 
int main (){
	
/*Se desea obtener el promedio de g grupos que est�n en un mismo a�o escolar; 
siendo que cada grupo puede tener n alumnos que cada alumno puede llevar m materias 
y que en todas las materias se promedian tres calificaciones,
para obtener el promedio de la materia. Lo que se desea desplegar es el promedio de los grupos, 
el promedio de cada grupo y el promedio de cada alumno.*/

	float a,b,c,g,n,m,c1,c2,c3,suma1=0,suma2=0,PA,PG,PGS; 
	
	printf ("Cuantos grupos se promediaran? "); scanf ("%f",&g);
	a=1;
	while (a<=g){
		printf ("\t\t Grupo %.0f",a);
		printf ("\n Cuantos alumnos tiene el grupo %.0f?",a); scanf ("%f",&n);
		b=1;
		while (b<=n){
			printf ("\nCuantas materias lleva el alumno %.0f",b); scanf ("%f",&m);
			c=1;
			while (c<=m){
				printf ("\t\t Promedio de califiacion de materia %.0f",c);
				
				printf ("\nCual fue la calificacion 1?"); scanf ("%f",&c1);
				printf ("\nCual fue la calificacion 2?"); scanf ("%f",&c2);
				printf ("\nCual fue la calificacion 3?"); scanf ("%f",&c3);
				
				PA = (c1+c2+c3)/3;
				
				printf ("\nTu calificacion de la materia %.0f es de: %f\n",c,PA);
				
				c++; 
			} 
			suma1 += PA;
			PG = suma1/n;
			printf ("Promedio grupo del grupo %.0f es de: %f",a,PG);	
			b++;
		}
						
		suma2 += PG;
		PGS = suma2/g;
		a++;
	}
	
	printf ("Promedio total de grupos es de: %.0f",PGS);
	
	return 0;
}
