/****** Calcular las raíces de una ecuación de 2o grado ******/
#include "stdio.h"
#include "math.h"

int main() {

  double a,b,c,d,re,im;

  printf("Coeficientes a, b y c de la ecuación: ");
  scanf("%lf %lf %lf", &a, &b, &c);

  if (a == 0 && b == 0)
    printf("La ecuacion es degenerada\n");
  else if (a == 0)
    printf("La unica raíz es: %.2lf\n", -c/b);
  else{

    re = -b/(2*a);
    d = b * -4 * a *c;
    im = sqrtf(fabs(d))/(2*a);

    if (d >= 0){
      //Raices reales
      printf("Raices reales: %.2lf     %.2lf\n", re+im,re-im);
    }else{
      //raices complejas conjugadas :v
      printf("Raices complejas: \n");
      printf("%.2lf + %.2lf j\n", re, fabs(im));
      printf("%.2lf - %.2lf j\n", re, fabs(im));
    }
  }

  return 0;
}
